clear
// set the consants 
r_w = 0.002 //wire radius [m]]
r_s= 0.0508//Spring Radius 
G = 80*10^9 //Steel Kirchoff's module [Pa]
m = 0.01 // Objects mass [kg] 
g = 9.81 //Gravitational accelaration [N/kg]
F = m*g // force [N]
Alpha=F/m
Tau = 2 // Damping constant [s]
Beta = 1/(2*Tau)
Na=25// number of coils 
f=[2:1:50] // range of Frequencys



// exercise a 
k=(G*r_w^4)/(4*Na*r_s)// spring constant 
omega=sqrt(k/m) // Resonant Frequency
j=1 //costant that increse in loop 
Amplitudes=zeros(1,length(f)) // marix that contain amplitudes, needed for future plotting, creating the matrix full of 0 is easier then making the the matrix larger and larger every new calculated amplitude 
while j  < length(f)
    Amplitudes(1,j)=Alpha/((omega^2-f(1,j)^2)^2+4*Beta*f(1,j)^2)// the function that calculates aplituteds, if you wonder what happend to sins part in this formula, its 1 that changes distance to max distance so its  amplitude 
    j=j+1
end
//disp([Amplitudes(1,1),Amplitudes(1,2)Amplitudes(1,3)Amplitudes(1,341)])// I used this part to check if everything is alright 

// exercise b 
Coils=[2:1:50]
f=[2:1:50] // range of Frequencys

// t creates matrix to save  values amplitudes for diffrent numbers coils and diffrent values of frequency
Amplitudes_N=zeros(length(f),length(Coils))

// this loop at first set the number of coils then calulates  through all of set frequences and then increse the coils, and calucatet again 
l=1
while l<=length(f)
    j=1
    while j <= length(Coils)
        k=(G*r_w^4)/(4*Coils(1,j)*r_s)
        omega_A=sqrt(k/m)
        Amplitudes_N(l,j)=Alpha/((omega_A^2-f(1,l)^2)^2+4*Beta*f(1,l)^2) 
        j=j+1
    end
        l=l+1
        
end
// plotting,
scf(1) 
subplot(221)// the first plot  this function creates two plots in one window 
plot(f,Amplitudes_N)
subplot(222) // and the second plot this function creates two plots in one window 
plot(Coils,Amplitudes)
xs2pdf(1,' resonance.pdf') // and this saves the current window ( it was set by scf function ) to pdf 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
